all: clean
	mkdir -p ./bin	
	erlc -o"./bin" -I"./src/" ./src/{dpll,dpll_original,fixpoint,safecomp,simple,common}.erl
test: all
	mkdir -p ./tests-output
	ct_run -dir ./tests -suite safecomp_SUITE -logdir ./tests-output -pa "./bin" -include "./src"
	ct_run -dir ./tests -suite dpll_SUITE -logdir ./tests-output -pa "./bin" -include "./src"
	ct_run -dir ./tests -suite dpll_fp_SUITE -logdir ./tests-output -pa "./bin" -include "./src"
shell:
	erl -pa ./bin
clean:
	rm -rf ./bin/*.beam
	rm -rf ./tests-output
