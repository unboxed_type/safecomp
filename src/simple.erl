%% A collection of simple iterative functions that converge to
%% the result, for testing.

-module(simple).
-export([sum/1, mult/1]).

%% sum numbers from 1 to N
%% call with initial point: {N, 0}
sum({0, Acc}) ->
    {0, Acc};
sum({N, Acc}) ->
    {N - 1, Acc + N}.

%% multiply numbers from 1 to N
%% call with initial point: {N, 1}
mult({1, Acc}) ->
    Acc;
mult({N, Acc}) ->
    {N - 1, Acc * N}.
