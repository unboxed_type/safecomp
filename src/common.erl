%% =============================================================================
%% Useful common routines needed both for protocol and for testing.
%% 
%% Author: evgeny.shishkin@infotecs.ru
%% Copyright (C) Infotecs JSC, 2020
%% =============================================================================

-module(common).
-export([bit/2
	, hash/1
	, hash_n/2
	, project_bits/1
	, next_cert_projection/2
	, part_cert/2
	, swap_random_bit/1
	, swap_random_bit_ext/1
	, swap_bit/2	 
	, count_bits/1
	, certificate_projection/1
	, ite/3
	]
       ).
-export([parse_dimacs/1]).
-export([parse_dimacs_line/4]).

bit(Value, Pos) ->
    (Value bsr Pos) band 1.

%% The chosen cryptographic hash function.
hash(Value) ->
    BigPrime = 4294967291,
    binary:decode_unsigned(
      crypto:hash(sha, term_to_binary(Value))) rem BigPrime.

%% Hash the value N times
hash_n(Value, 0) ->
    Value;
hash_n(Value, N) ->
    hash_n(hash(Value), N - 1).

%% Calculate a part of the certificate, according to 
%% protocol rule
part_cert(Value, PrevPartCert) ->
    hash([PrevPartCert, Value]).

project_bits(Cert) ->
    Cert band 1.

next_cert_projection(Cert, ProjCert) ->
    (Cert bsl 1) band project_bits(ProjCert).

%% FullCert = list of integers, partial certificates of each F call
%% and the H(D) for the C_0. 
certificate_projection(FullCert) ->
    R = lists:reverse(FullCert),
    C0 = hd(R),
    Tail = lists:reverse(tl(R)),
    lists:foldr(
      fun(E, Acc) -> 
	      next_cert_projection(E, Acc) 
      end,
      C0, 
      Tail
     ).

swap_random_bit(Value) when is_integer(Value) ->
    TotalBits = count_bits(Value),
    N = rand:uniform(TotalBits) - 1,
    swap_bit(Value, N).

swap_random_bit_ext(Value) when is_integer(Value) ->
    TotalBits = count_bits(Value),
    N = rand:uniform(TotalBits) - 1,
    {swap_bit(Value, N), N}.
    
%% N : bit number, zero-based
swap_bit(Value, N) when is_integer(Value) ->
    IsSet = bit(Value, N) == 1,
    case IsSet of
	true -> 
	    Value band (bnot (1 bsl N));
	false ->
	    Value bor (1 bsl N)
    end.

%% Calculates how many bits are needed to store Value
%% NOTE: Bitshifting algorithm might be much faster.
count_bits(0) ->
    1;
count_bits(Value) ->
    erlang:round(math:ceil(math:log2(Value + 1))).

ite(Cond, Then, _Else) when Cond == true ->
    Then;
ite(_, _, Else) ->
    Else.

to_int(Str) ->
    {X, []} = string:to_integer(Str),
    X.

%% {V, C, D} = dpll:parse_dimacs("test.cnf").
parse_dimacs(Path) ->
    {ok, F} = file:open(Path, read),
    {ok, Head} = file:read_line(F),
    ["p","cnf", Vars, Clauses] = string:split(string:trim(Head), " ", all),
    parse_dimacs_line(F, [], to_int(Vars), to_int(Clauses)).

parse_dimacs_line(F, Result, Vars, Clauses) ->
    case file:read_line(F) of
	{ok, L} ->
	    Values = lists:droplast(
		       string:split(string:trim(L), " ", all)
		      ),
	    Ints = [to_int(Y) || Y <- Values],
	    parse_dimacs_line(F, [Ints | Result], Vars, Clauses);
	eof ->
	    ok = file:close(F),
	    {Vars, Clauses, Result}
    end.
