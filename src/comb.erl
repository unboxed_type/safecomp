-module(comb).
-export([comb/2,comb2/1]).
-export([comb_stack/1]).

comb(1, _) -> 1;
comb(_, 0) -> 1;
comb(N, N) -> 1;
comb(N, M) ->
    A = comb(N - 1, M),
    B = comb(N - 1, M - 1),
    A + B.

%% comb_stack
comb_map({1, _}) -> 1;
comb_map({_, 0}) -> 1;
comb_map({N, N}) -> 1;
comb_map(X) -> X.

comb_stack([A]) when is_integer(A) ->
    A;
comb_stack([A | [{_,_}=B | T]]) when is_integer(A) ->
    comb_stack([B | [A | T]]);
comb_stack([A | [B | [sum | T]]]) 
  when is_integer(A) andalso is_integer(B) ->
    comb_stack([A + B | T]);
comb_stack([{N, M} | T]) ->
    comb_stack([comb_map({N - 1, M}) | 
		[comb_map({N - 1, M - 1}) | 
		 [sum | T]
		]
	       ]
	      ).


%% cps version
%% comb_cps(N, M) ->
%%     comb_cps_1({N, M}, fun comb_cps_2/2).

%% comb_cps_1({1, _}, C) ->
%%     C(1, fun comb_cps_3/2);
%% comb_cps_1({_, 0}, C) ->
%%     C(1, fun comb_cps_3/2);
%% comb_cps_1({N, N}, C) ->
%%     C(1, fun comb_cps_3/2);

%% comb_cps_2(A, C) ->
%%     C({A, N - 1, M - 1}, fun comb_cps_
    
%% fixpoint friendly version.
%% We unfold each C(N,M) call until we reach base case
comb2({[], Acc}) ->
    {[], Acc};
comb2({[1 | T], Acc}) ->
    {T, 1 + Acc};	  
comb2({[{1, _} | T], Acc}) ->
    {T, 1 + Acc};
comb2({[{_, 0} | T], Acc}) ->
    {T, 1 + Acc};
comb2({[{P1, P1} | T], Acc}) ->
    {T, 1 + Acc};
comb2({[{P1, P2} | T], Acc}) ->
    {[{P1 - 1, P2} | [{P1 - 1, P2 - 1} | T]], Acc}.
