%% =============================================================================
%% Iterative fixpoint-friendly version of the classic DPLL algorithm.
%% 
%% Author: evgeny.shishkin@infotecs.ru
%% Copyright (C) Infotecs JSC, 2020
%% =============================================================================

-module(dpll).
-export([dpll_fp/1, del/2, cnd/2, unit_resolution/2, choose_literal/1]).
% we denote each symbol with natural number, starting from 1
% we denote negation with minus sign.
% D = {{not A, not B}, {B, C}, {not C, D}, {A}}
% D = [[-1, -2], [2, 3], [-3, 4], [1]]

del(C, L) ->
    lists:delete( (-L), C).
				   
% Conditioning operation of formula F on symbol L
% Substitute every symbol (~L) with 'true', remove every clause containing L
% "Handbook of satisfiability - Biere et al. 2009", p.116
cnd(F, L) ->
    [del(Y, L) || Y <- F, not lists:member(L, Y)].

%% returns (I, G) where: 
%% I = a set of existing or derived unit-literals substituted.
%% G = a new CNF which results from conditioning on literals I
%% set of literals I and G must not intersect
unit_resolution(F, Units0) ->
    Units = [hd(X) || X <- F, length(X) == 1],
    F1 = lists:foldl(
	   fun(Elem, Acc) -> cnd(Acc, Elem) end,
	   F, 
	   Units),
    case F1 == F of
	true ->
	    {Units ++ Units0, F1};
	false ->
	    unit_resolution(F1, Units ++ Units0)
    end.

choose_literal(F) when is_list(F) ->
    choose_literal(F, first).

choose_literal(F, _) when is_integer(F) ->
    F;

choose_literal(F, first) when is_list(F) ->
    choose_literal(hd(F), first);

%% Randomly choose an integer element from a list F
%% An element may be a list by itself, in this case 
%% we do a recursive call.
choose_literal(F, random) ->
    N = rand:uniform(length(F)),
    choose_literal(lists:nth(N, F), random).

%% ===========================================
%% fixpoint-friendly version of DPLL
%% ===========================================
dpll_fp([{ur, F} | T]) ->
    {I, G} = unit_resolution(F, []),
    case G == [] of
	true ->
	    [{result, I} | T];
	false ->
	    case lists:member([], G) of
		true ->
		    [{result, unsat} | T];
		false ->
		    Lit = choose_literal(G),
		    Cnd1 = cnd(G, Lit),
		    [{ur, Cnd1} | [{dpll2_tail, G, Lit, I} | T]]
	    end
    end;
dpll_fp([{result, unsat} | [{dpll2_tail, G, Lit, I} | T]]) ->
    Cnd2 = cnd(G, (-Lit)),
    [{ur, Cnd2} | [{dpll3_tail, Lit, I} | T]];
dpll_fp([{result, R} | [{dpll2_tail, _G, Lit, I} | T]]) ->
    [{result, [Lit | R ++ I]} | T];
dpll_fp([{result, unsat} | [{dpll3_tail, _Lit, _I} | T]]) ->
    [{result, unsat} | T];
dpll_fp([{result, R} | [{dpll3_tail, Lit, I} | T]]) ->
    [{result, [(-Lit) | R ++ I]} | T];
dpll_fp([{result, _}]=L) ->
    L.

%% SAT tests:
%% dpll:dpll_fp([[-1, 2], [-2, 3]]).
%% dpll:dpll_fp([[-1, -2], [2, 3], [-3, 4], [1]]).
%% dpll:dpll_fp([[-1, -2], [2, 3], [-3, 4], [3]]).

%% f(C),f(V),f(D),{C,V,D}=common:parse_dimacs("./test.cnf").
%% f(R), [{result, R}] = fixpoint:fixpoint(fun dpll:dpll_fp/1, [{ur, D}]).
%% lists:usort(R) == lists:usort(dpll:dpll(D)).

%% f(C),f(V),f(D),{C,V,D}=common:parse_dimacs("./queueinvar2.dimacs").
%% f(R), timer:tc(fun() -> {_, R} = fixpoint:fixpoint_ext(fun dpll:dpll_fp/1, [{ur, D}], {0, 0, 0}), R end).
%% {410801,170758}
