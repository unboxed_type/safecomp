-module(sort).
-export([sort/1, fsort/1, fpsort/1, merge/2]).

%% ====================================================
%% classic merge sort
%% ====================================================
merge([], R) ->
    R;
merge(L, []) ->
    L;
merge([H0 | T0], [H1 | T1]=R) when H0 =< H1 ->
    [H0 | merge(T0, R)];
merge(L, [H1 | T1]) ->
    [H1 | merge(L, T1)].

sort(S) when length(S) =< 1 -> 
    S;
sort(S) ->
    {L, R} = lists:split(length(S) div 2, S),
    merge(sort(L), sort(R)).

%% ====================================================
%% fixpoint-friendly merge sort 
%% ====================================================
fsort(S) ->
    element(2, fixpoint:fixpoint(fun fpsort/1, {S, [], split2})).

%% split phase
fpsort({[], Acc, split2}) ->
    {Acc, [], merge2};
fpsort({[E], Acc, split2}) ->
    {[[E] | Acc], [], merge2};
fpsort({[H0 | [H1 | T0]]=S, Acc, split2}) when H0 =< H1 ->
    {T0, [[H0, H1] | Acc], split2};
fpsort({[H0 | [H1 | T0]]=S, Acc, split2}) ->
    {T0, [[H1, H0] | Acc], split2};

%% merge phase
fpsort({[], Acc, merge2}) ->
    {[], Acc, merge2}; % fixpoint reached!
fpsort({[H0 | T], Acc, merge2}) ->
    {T, merge(H0, Acc), merge2}.
