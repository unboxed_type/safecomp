%% Vars denote the number of variables.
naive([[]], _) ->
    false;
naive([], _) ->
    true;
naive(F, Vars) ->
    naive(cnd(F, Vars), Vars - 1).

dpll_minus([], _) ->
    [];
dpll_minus(F, D) ->
    case lists:member([], F) of 
	true -> 
	    unsat;
	false ->
	    dpll_minus_(F, D)
    end.
dpll_minus_(F, D) ->
    case dpll_minus(cnd(F, D + 1), D + 1) of
	unsat ->
	    case dpll_minus(cnd(F, -(D + 1)), D + 1) of
		unsat ->
		    unsat;
		X ->
		    [ -(D + 1) | X ]
	    end;
	Y ->
	    [ (D + 1) | Y ]
    end.

