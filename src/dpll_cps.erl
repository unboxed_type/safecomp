-module(dpll_cps).
-export([dpll_cps/1]).

% Conditioning operation of formula F on symbol L
% Substitute every symbol (~L) with 'true', remove every clause containing L
% "Handbook of satisfiability - Biere et al. 2009", p.116
del(C, L) ->
    lists:delete( (-L), C).
cnd(F, L) ->
    [del(Y, L) || Y <- F, not lists:member(L, Y)].

%% returns (I, G) where: 
%% I = a set of existing or derived unit-literals substituted.
%% G = a new CNF which results from conditioning on literals I
%% set of literals I and G must not intersect
unit_resolution(F, Units0) ->
    Units = [hd(X) || X <- F, length(X) == 1],
    F1 = lists:foldl(
	   fun(Elem, Acc) -> cnd(Acc, Elem) end,
	   F, 
	   Units),
    case F1 == F of
	true ->
	    {Units ++ Units0, F1};
	false ->
	    unit_resolution(F1, Units ++ Units0)
    end.

choose_literal(F) when is_list(F) ->
    choose_literal(F, first).

choose_literal(F, _) when is_integer(F) ->
    F;

choose_literal(F, first) when is_list(F) ->
    choose_literal(hd(F), first);

%% Randomly choose an integer element from a list F
%% An element may be a list by itself, in this case 
%% we do a recursive call.
choose_literal(F, random) ->
    N = rand:uniform(length(F)),
    choose_literal(lists:nth(N, F), random).

%% entry point
dpll_cps(F) ->
    dpll_cps_(F, fun dpll_ur_cps/1).
dpll_ur_cps(R) ->	      
    dpll_ur_(R, fun dpll_body_cps/1).
dpll_body_cps(R) ->
    dpll_body_(R, fun dpll_cps/1).
dpll_body2_cps(R) ->
    dpll_body2_(R, 

dpll_cps_(F, C) ->
    {I, G} = unit_resolution(F, []),
    C({I, G}).
    
dpll_ur_({I,[]}, C) ->
    C(I);
dpll_ur_({I, G}, C) ->
    case lists:member([], G) of
	true ->
	    C(unsat);
	false ->
	    C({I, G})
    end.

dpll_body_({I, G}, C) ->
    Lit = choose_literal(G),
    Cnd1 = cnd(G, Lit),
    dpll_cps_(Cnd1, fun dpll_body2_cps/1).

dpll_body2_({unsat, G, Lit}, C) ->
    Cnd2 = cnd(G, (-Lit)),
    C(Cnd2);
dpll_body2_({L, G, Lit}, C) ->
    C([Lit | L ++ I]).

dpll_body3_(unsat, C) ->
    C(unsat);
dpll_body3_({L, Lit, I}, C) ->
    C([(-Lit) | L ++ I]).
