-module(dpll_original).
-export([dpll/1]).

% Conditioning operation of formula F on symbol L
% Substitute every symbol (~L) with 'true', remove every clause containing L
% "Handbook of satisfiability - Biere et al. 2009", p.116
del(C, L) ->
    lists:delete( (-L), C).
cnd(F, L) ->
    [del(Y, L) || Y <- F, not lists:member(L, Y)].

%% returns (I, G) where: 
%% I = a set of existing or derived unit-literals substituted.
%% G = a new CNF which results from conditioning on literals I
%% set of literals I and G must not intersect
unit_resolution(F, Units0) ->
    Units = [hd(X) || X <- F, length(X) == 1],
    F1 = lists:foldl(
	   fun(Elem, Acc) -> cnd(Acc, Elem) end,
	   F, 
	   Units),
    case F1 == F of
	true ->
	    {Units ++ Units0, F1};
	false ->
	    unit_resolution(F1, Units ++ Units0)
    end.

choose_literal(F) when is_list(F) ->
    choose_literal(F, first).

choose_literal(F, _) when is_integer(F) ->
    F;

choose_literal(F, first) when is_list(F) ->
    choose_literal(hd(F), first);

%% Randomly choose an integer element from a list F
%% An element may be a list by itself, in this case 
%% we do a recursive call.
choose_literal(F, random) ->
    N = rand:uniform(length(F)),
    choose_literal(lists:nth(N, F), random).

%% Classic DPLL algorithm
%% see "Biere et al - Handbook of Satisfiability (2009)", p. 129
dpll(F) ->
    {I, G} = unit_resolution(F, []),
    dpll_ur({I, G}).

dpll_ur({I, []}) ->
    I;
dpll_ur({I, G}) ->
    case lists:member([], G) of
	true -> unsat;
	false -> dpll_body({I, G})
    end.
dpll_body({I, G}) ->
    Lit = choose_literal(G),
    case dpll(cnd(G, Lit)) of
	unsat ->
	    case dpll(cnd(G, (-Lit))) of
		unsat ->
		    unsat;
		L -> 
		    [(-Lit) | L ++ I]
	    end;
	L ->
	    [Lit | L ++ I]
    end.
