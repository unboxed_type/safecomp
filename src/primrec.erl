-module(primrec).
-export([ite/3, eq/2, le/2, lt/2, gt/2, minus/2, minus1/1, fact/1, sum/2, mult/2]).

%% primitive recursive operator
%h(X, 0) = Rec0;
%h(X, Y) ->
%    g(X, Y, h(X, Y - 1)).

%% primitive recursive function
inc(X) ->
    X + 1.

sum(X, 0) ->
    X;
sum(X, Y) ->
    sum(inc(X), Y - 1).

mult(_X, 0) ->
     0;
mult(X, Y) ->
     sum(X, mult(X, Y - 1)).

fact(0) -> 
     1;
fact(Y) ->
     mult(Y, fact(Y - 1)).

minus1(0) ->
    0;
minus1(X) ->
    X - 1.

minus(0, _X2) ->
    0;
minus(X1, 0) ->
    X1;
minus(X1, X2) ->
    minus(minus1(X1), minus1(X2)).

gt(0, 0) ->
    false;
gt(_X1, 0) ->
    true;
gt(X1, X2) ->
    gt(minus(X1, X2), 0).

le(X1, X2) ->
    not(gt(X1, X2)).

eq(0, 0) ->
    true;
eq(_X1, 0) ->
    false;
eq(0, _X2) ->
    false;
eq(X1, X2) ->
    eq(minus1(X1), minus1(X2)).

lt(X1, X2) ->
    le(X1, X2) andalso not(eq(X1, X2)).

ite(true, X2, _X3) ->
    X2;
ite(false, _X2, X3) ->
    X3.
