%% =============================================================================
%% Safecomp protocol proof-of-concept main module.
%% Smart-contract is simulated with statem state machine that is able to
%% process user requests.
%%
%% Author: evgeny.shishkin@infotecs.ru
%% Copyright (C) Infotecs JSC, 2020
%% =============================================================================

-module(safecomp).
-behaviour(gen_statem).

-export([start/0, stop/0]).
-export([terminate/3, code_change/4, init/1, callback_mode/0]).
-export([submit/2, solve/2, status/1, solution/1,
	 refute/2, approve/2, tasks/0]).
-export([handle_event/4]).

-include_lib("stdlib/include/assert.hrl").
-include_lib("common_test/include/ct.hrl").
-define(INFO, ?LOW_IMPORTANCE).
-define(ERROR, ?HI_IMPORTANCE).

%% 1 second = 1 000 000 microseconds
-define(APPROVAL_PERIOD_MICROSEC, 60 * 60 * 1000000).

%% Status agenda:
%% initial = no task has been submitted
%% submitted = a task has been submitted
%% solved = someone provided a solution for the task
%% approved = one or several approvers sent their approvals
%%            for the task, within the approval period.
%%            Within the same period someone can provide 
%%            a refutation for the solution. 
%% finalized = Safecomp smart contract checked the correctness
%%             of approvers data and added the solution to its registry

-type status() :: initial | submitted | solved | approved | finalized.
-type timestamp() :: {integer(), integer(), integer()}.
-type point() :: binary().
-type task_fun() :: fun().
-type result() :: binary().
-type cert_proj() :: integer().
-type task_id() :: pos_integer().
-type pos() :: pos_integer().
-type user_id() :: pos_integer().

-record(solution, {result :: result(),
		   cert_proj :: cert_proj(),
		   hs :: binary()  %% hidden secret
		  }
       ).
-record(refutation, {pos :: pos(),
		     point :: point(),
		     part_cert :: pos_integer()
		    }
       ).
% -type part_id() :: pos_integer().
% -type balance() :: pos_integer().

-record(data, {id :: task_id(), 
	       task :: task_fun(),
	       point :: point(), 
	       status :: status(), 
	       ts :: timestamp(),
	       solution :: #solution{},
	       approvers :: list(user_id())
	      }
       ).

name() -> 
    safecomp.

new_data() ->
    #data{id = 1, status = initial}.

start() ->
    gen_statem:start({local, name()}, ?MODULE, [], []).

stop() ->
    gen_statem:stop(name()).

submit(TaskFun, Point) ->
    gen_statem:call(name(), {submit, TaskFun, Point}).

solve(Id, Solution=#solution{}) ->
    gen_statem:call(name(), {solve, Id, Solution}).

approve(Id, HS) ->
    gen_statem:call(name(), {approve, Id, HS}).

%% return a list of tasks waiting to be solved by
%% external actors.
tasks() ->
    gen_statem:call(name(), tasks).

%% get status of the task Id
status(Id) ->
    gen_statem:call(name(), {status, Id}).

%% Get proposed solution for the task Id
%% The solution comes in a form of a certificate projection.
solution(Id) ->
    gen_statem:call(name(), {solution, Id}).

%% disprove a proposed solution
refute(Id, Proof) ->
    % ct:print(?INFO, "refute(~p, ~p)", [Id, Proof]),
    {Pos, Point, PartCert} = Proof,
    Refutation = #refutation{pos=Pos, point=Point, part_cert=PartCert},
    gen_statem:call(name(), {refute, Id, Refutation}).

terminate(_Reason, _State, _Data) ->
    void.

code_change(_Vsn, State, Data, _Extra) ->
    {ok, State, Data}.

init([]) ->
    {ok, initial, new_data()}.

callback_mode() -> 
    handle_event_function.

handle_event({call, From}, {submit, F, D}, initial, Data) ->
    Id = Data#data.id,
    Data1 = 
	Data#data{task=F, point=D, id=Id, 
		  ts=erlang:timestamp(), 
		  status=submitted
		 },
    {next_state, submitted, Data1, [{reply, From, {ok, Id}}]};

handle_event({call, From}, {submit, _, _}, State, Data) ->
    {next_state, State, Data, [{reply, From, busy}]};

handle_event({call, From}, {solve, Id, Sol}, submitted, Data) ->
    ?assert(Id == Data#data.id),
    Data1 = Data#data{status=solved, solution=Sol},
    {next_state, solved, Data1, [{reply, From, ok}]};

handle_event({call, From}, {solve, _Id, _Sol}, solved, Data) ->    
    {next_state, solved, Data, [{reply, From, already_solved}]};

handle_event({call, From}, {status, Id}, State, Data) ->
    ?assert(Id == Data#data.id),
    {next_state, State, Data, [{reply, From, Data#data.status}]};

handle_event({call, From}, {solution, Id}, solved, Data) ->
    ?assert(Id == Data#data.id),
    {next_state, solved, Data, [{reply, From, Data#data.solution}]};

handle_event({call, From}, {refute, Id, Refutation}, solved, Data) ->
    ?assert(Id == Data#data.id),
    #data{task=F, point=D, solution=Solution} = Data,
    #solution{cert_proj=CertificateProjection} = Solution,
    case check_refutation_validity(F, D, CertificateProjection, Refutation) of
	true ->
	    Data1 = 
		Data#data{status=submitted, 
			  solution=undefined, 
			  ts=undefined
			 },
	    {next_state, submitted, Data1, [{reply, From, true}]};
	false ->
	    {next_state, solved, Data, [{reply, From, false}]}
    end;

handle_event({call, From}, tasks, submitted, Data) ->
    TaskId = Data#data.id,
    TaskFun = Data#data.task,
    TaskPt = Data#data.point,
    {next_state, submitted, Data, 
     [{reply, From, [{TaskId, TaskFun, TaskPt}]}]
    };

handle_event({call, From}, tasks, State, Data) ->
    {next_state, State, Data, [{reply, From, []}]};

handle_event({call, From}, {approve, TaskId, UserId, _UserHS}, solved, Data) ->
    %% UserHS = H( HS + UserId )
    %% This check will decide if the user is really an approver and knows
    %% correct HS
    ?assert(TaskId == Data#data.id),
    ElapsedTimeMicro = timer:now_diff(erlang:now(), Data#data.ts),
    ApprList = Data#data.approvers,
    Data2 = Data#data{status=approved, approvers=[UserId | ApprList]},
    RepliesOk = [{reply, From, ok}],
    case ElapsedTimeMicro >= ?APPROVAL_PERIOD_MICROSEC of
	true -> 
	    {next_state, approved, Data2, RepliesOk};
	false ->
	    RepliesError = [{reply, From, {error, approval_period}}],
	    {next_state, solved, Data, RepliesError}
    end;

handle_event(_, _, State, Data) ->
    {next_state, State, Data}.

%% Refutation = {Pos, Point, PartCertI}
%% Pos = iteration number of F^i(D) such that F^i(D) + PartCertI
%%       still agrees with the certificate, but the I + 1 projection
%%       does not agree with the certificate projection.
%% Pos = 0 if disagreement starts with the very first bit of the
%%         certificate projection.
check_refutation_validity(F, D, Solution, #refutation{pos=undefined}) ->
    PartCert0 = common:hash(D),
    CB1 = common:bit(Solution, 0),
    PartCert1 = common:part_cert(F(D), PartCert0),
    CB2 = common:project_bits(PartCert1),
    %ct:print(?INFO, "common:bit(~p, ~p) = ~p, common:bit(~p, 0) = ~p~n", 
    %	     [Solution, 0, CB1, PartCert1, CB2]),
    CB1 /= CB2;

check_refutation_validity(F, _D, Solution, Refutation) ->
    #refutation{pos=Pos, point=Point, part_cert=PartCert0} = Refutation,
    %ct:print(?INFO, "Refutation = ~p, PartCert0 = ~p~n", [Refutation, PartCert0]
	%    ),
    CB1 = common:bit(Solution, Pos),
    CB2 = common:project_bits(PartCert0),
    %ct:print(?INFO, "common:bit(~p, ~p) = ~p, common:bit(~p, 0) = ~p~n", 
    %	     [Solution, Pos, CB1, PartCert0, CB2]),
    case CB1 == CB2 of
	false ->
	    false;
	true ->
	    InvalidBitPos = Pos + 1,
	    F2 = F(Point),
	    PartCert1 = common:part_cert(F2, PartCert0),
	    CB3 = common:bit(Solution, InvalidBitPos),
	    CB4 = common:project_bits(PartCert1),
	    %ct:print(?INFO, "common:bit(~p, ~p) = ~p, common:bit(~p, 0) = ~p~n", 
		%     [Solution, InvalidBitPos, CB3, PartCert1, CB4]),
	    not (CB3 == CB4)
    end.
