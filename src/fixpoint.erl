-module(fixpoint).
-export([fixpoint/2, fixpoint_cert/2]).

%% this function is added for more convenient testing
-export([fixpoint_cert_test/2]).

-include_lib("stdlib/include/assert.hrl").
-include_lib("common_test/include/ct.hrl").
-define(INFO, ?LOW_IMPORTANCE).
-define(ERROR, ?HI_IMPORTANCE).

%% Find a fixpoint of the function F starting from the point D
fixpoint(F, D) ->
    R = F(D),
    case R == D of
	true -> R;
	false -> fixpoint(F, R)
    end.

%% Find a fixpoint and calculate a projected certificate for this computation.
fixpoint_cert(F, D) ->
    PartCert0 = common:hash(D),
    fixpoint_cert_(F, D, [PartCert0]).

fixpoint_cert_(F, D, PartCertList) ->
    R = F(D),
    case R == D of
	true -> 
	    {R, PartCertList};
	false ->
	    [PartCert0 | _] = PartCertList,
	    PartCert1 = common:part_cert(D, PartCert0),
	    fixpoint_cert_(F, R, [PartCert1 | PartCertList])
    end.

fixpoint_cert_test(F, D) ->
    PartCert0 = common:hash(D),
    fixpoint_cert_test_(F, D, [PartCert0], erlang:external_size(D)).

fixpoint_cert_test_(F, D, PartCertList, Size) ->
    R = F(D),    
    case R == D of
	true -> 
	    {R, PartCertList, Size};
	false ->
	    [PartCert0 | _] = PartCertList,
	    PartCert1 = common:part_cert(D, PartCert0),
	    RSize = erlang:external_size(R),
	    Size1 = common:ite(RSize > Size, RSize, Size),
	    fixpoint_cert_test_(F, R, [PartCert1 | PartCertList], Size1)
    end.
