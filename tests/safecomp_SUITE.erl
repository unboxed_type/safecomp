-module(safecomp_SUITE).
-include_lib("common_test/include/ct.hrl").
-include_lib("stdlib/include/assert.hrl").
-define(INFO, ?LOW_IMPORTANCE).
-define(ERROR, ?HI_IMPORTANCE).
-define(TEST_FILE, "queueinvar2.dimacs").

-export([all/0
	, init_per_testcase/2
	, end_per_testcase/2
	, suite/0
	]).

-export([test_submit/1
	, test_status/1
	, test_double_submit/1
	, test_refutation1/1
	, test_refutation2/1
	]).

all() ->
    [test_submit
    , test_status
    , test_double_submit
    , test_refutation1
    , test_refutation2
    ].

suite() ->
    [{timetrap, {minutes, 60*24}}].

init_per_testcase(_TestCase, Config) ->
    safecomp:start(),
    Config.

end_per_testcase(_TestCase, Config) ->
    safecomp:stop(),
    Config.

test_data(Config) ->
    custom_data(Config, ?TEST_FILE).

custom_data(Config, Path) ->
    {_, _, D} = common:parse_dimacs(
		  ?config(data_dir, Config) ++ Path
		 ),
    D.
    

test_submit(_Config) ->
    D = test_data(_Config),
    {ok, _Id} = safecomp:submit(fun dpll:dpll_fp/1, D).

test_status(_Config) ->
    D = test_data(_Config),
    {ok, Id} = safecomp:submit(fun dpll:dpll_fp/1, D),
    ?assert(safecomp:status(Id) == submitted).

test_double_submit(_Config) ->
    D = test_data(_Config),
    {ok, _Id} = safecomp:submit(fun dpll:dpll_fp/1, D),
    ?assert(safecomp:submit(fun dpll:dpll_fp/1, D) == busy).
    
test_refutation1(_Config) ->
    StartPoint = {1000, 0},
    Solution = fixpoint:fixpoint_cert(fun simple:sum/1, StartPoint), % r_n
    {_Result, FullCert} = Solution,
    HHS = common:hash_n(FullCert, 2), % hc
    CertProjection = common:certificate_projection(FullCert), % cp
    % ct:print(?INFO, "Full solution = ~p~n", [Solution]),
    % submit a problem
    {ok, Id} = safecomp:submit(fun simple:sum/1, StartPoint),
    {InvalidCertProj, BitPos} = common:swap_random_bit_ext(CertProjection),
    IncorrectResult = {0, 100},
    InvalidHHS = common:swap_random_bit(HHS),
    IncorrectSolution = 
	{solution, IncorrectResult, InvalidCertProj, InvalidHHS},
    %ct:print(?INFO, "swap_random_bit_ext(~p) = ~p~n", 
    %	     [CertProj, {InvalidCertProj, BitPos}]),
    ?assert(BitPos < length(FullCert)),
    % ct:print(?INFO, "BitPos = ~p~n", [BitPos]),
    % submit an incorrect solution to the problem
    ok = safecomp:solve(Id, IncorrectSolution),
    % refute with a proof
    case BitPos == 0 of
	true ->
	    ?assert(safecomp:refute(Id, {undefined, undefined, undefined}));
	false ->
	    PartCertsLength = length(FullCert),
	    {PartCert1, Point} = lists:nth(PartCertsLength - (BitPos - 1), FullCert),
	    % ct:print(?INFO, "Refutation is made with = ~p~n", [{PartCert1, Point}]),
	    ?assert(safecomp:refute(Id, {BitPos - 1, Point, PartCert1}))
    end.

test_refutation2(_Config) ->
    Point = {4, 0},
    FP = fixpoint:fixpoint_cert(fun simple:sum/1, Point),
    {Result, FullCert} = FP,
    HHS = common:hash_n(FullCert, 2),
    CertProjection = common:certificate_projection(FullCert),
    % ct:print(?INFO, "Full solution = ~p~n", [Solution]),
    % submit a problem
    {ok, Id} = safecomp:submit(fun simple:sum/1, Point),
    % submit an incorrect solution to the problem
    Solution = {solution, Result, CertProjection, HHS},
    ok = safecomp:solve(Id, Solution),
    % refute with a proof
    ?assert(not(safecomp:refute(Id, {0, Point, CertProjection}))).
