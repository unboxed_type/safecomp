-module(fixpoint_SUITE).
-include_lib("common_test/include/ct.hrl").
-include_lib("stdlib/include/assert.hrl").
-define(INFO, ?LOW_IMPORTANCE).
-define(ERROR, ?HI_IMPORTANCE).
-export([all/0]).

-export(
   [
    test_merge1/1, 
    test_merge2/1,
    test_sort1/1,
    test_sort2/1,
    test_sort3/1,
    test_fsort1/1,
    test_fsort2/1,
    test_fsort3/1,
    test_fsort4/1,
    test_fsort5/1,
    test_fsort6/1,
    test_fsort_time_measure/1
   ]).

all() ->
    [
     test_merge1
    , test_merge2
    , test_sort1
    , test_sort2
    , test_sort3
    , test_fsort1
    , test_fsort2
    , test_fsort3
    , test_fsort4
    , test_fsort5
    , test_fsort6
    , test_fsort_time_measure
    ].
    
test_merge1(_Config) ->
    ?assert(sort:merge([], [1,2,3]) == [1,2,3]),
    ?assert(sort:merge([2,1], []) == [2,1]).

test_merge2(_Config) ->
    ?assert(sort:merge([2,1,3], [1,5]) == [1,2,1,3,5]).

test_sort1(_Config) ->
    ?assert(sort:sort([]) == []),
    ?assert(sort:sort([1]) == [1]).

test_sort2(_Config) ->
    ?assert(sort:sort([1,2]) == [1,2]),
    ?assert(sort:sort([1,2,3]) == [1,2,3]).

test_sort3(_Config) ->
    ?assert(sort:sort([3,2,1]) == [1,2,3]),
    ?assert(sort:sort([3,2,1,4,5]) == [1,2,3,4,5]).

test_fsort1(_Config) ->
    ?assert(sort:fsort([]) == []),
    R = sort:fsort([1]),
    %ct:print(?INFO, "R = ~p~n", [R]),
    ?assert(R == [1]).

test_fsort2(_Config) ->
    R = sort:fsort([1,2]),
    %ct:print(?INFO, "R = ~p~n", [R]),
    ?assert(R == [1,2]),
    R1 = sort:fsort([1,2,3]),
    %ct:print(?INFO, "R = ~p~n", [R1]),
    ?assert(R1 == [1,2,3]).

test_fsort3(_Config) ->
    R = sort:fsort([3,2,1]),
    % ct:print(?INFO, "R = ~p~n", [R]),    
    ?assert(R == [1,2,3]).

test_fsort4(_Config) ->
    R = sort:fsort([3,2,1,4,5]),
    %ct:print(?INFO, "R = ~p~n", [R]),
    ?assert(R == [1,2,3,4,5]).

test_fsort5(_Config) ->
    R = sort:fsort([10,5,12,4,14,5,3,12,10,14]),
    %ct:print(?INFO, "R5 = ~p~n", [R]),
    ?assert(R == [3,4,5,5,10,10,12,12,14,14]).

test_fsort_time_measure(_Config) ->
    N = 10000,
    RandomLists = [rand_list() || _ <- lists:seq(1, N)],
    SortT1 = erlang:timestamp(),
    _Result1 = [sort:sort(L) || L <- RandomLists],
    SortT2 = erlang:timestamp(),
    _Result2 = [sort:fsort(L) || L <- RandomLists],
    SortT3 = erlang:timestamp(),
    ct:print(?INFO, "Running sort took ~p microsec~n", [timer:now_diff(SortT2, SortT1)]),
    ct:print(?INFO, "Running fsort took ~p microsec~n", [timer:now_diff(SortT3, SortT2)]), 
    ok.

rand_list() ->
    N = rand:uniform(100),
    rand_list(N).

rand_list(N) ->
    rand_list_(N, []).
    
rand_list_(0, Acc) ->
    Acc;
rand_list_(N, Acc) ->
    rand_list_(N - 1, [rand:uniform(20) | Acc]).

test_fsort6(_Config) ->
    NumOfRandomLists = 10,
    RandomLists = [rand_list() || _ <- lists:seq(1, NumOfRandomLists)],
    Result1 = [sort:sort(L) || L <- RandomLists],
    Result2 = [sort:fsort(L) || L <- RandomLists],
    %ct:print(?INFO, "RL = ~p~n,R1 = ~p~nR2= ~p~n", [RandomLists, Result1, Result2]),    
    ?assert(Result1 == Result2).
