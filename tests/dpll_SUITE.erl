-module(dpll_SUITE).
-include_lib("common_test/include/ct.hrl").
-include_lib("stdlib/include/assert.hrl").
-define(INFO, ?LOW_IMPORTANCE).
-define(ERROR, ?HI_IMPORTANCE).

-export([all/0
	, init_per_testcase/2
	, end_per_testcase/2
	, suite/0
	]).

-export([test_queue_invar2/1
	, test_queue_invar4/1
	, test_bmc_ibm5/1
	, test_bmc_ibm6/1
	, test_bmc_ibm7/1
	, test_queue_invar6/1
	]).

all() ->
    [test_queue_invar2
    , test_queue_invar4
%    , test_bmc_ibm5
%    , test_bmc_ibm7
%    , test_bmc_ibm6
%    , test_queue_invar6
    ].

suite() ->
    [{timetrap, {minutes, 60*3}}].

init_per_testcase(_TestCase, Config) ->
    Config.

end_per_testcase(_TestCase, Config) ->
    Config.

custom_data(Config, Path) ->
    {_, _, D} = common:parse_dimacs(
		  ?config(data_dir, Config) ++ Path
		 ),
    D.

test_bmc_ibm5(_Config) ->
    test_dpll_original( _Config, "./bmc-ibm-5.cnf").

test_bmc_ibm6(_Config) ->
    test_dpll_original( _Config, "./bmc-ibm-6.cnf").

test_bmc_ibm7(_Config) ->
    test_dpll_original( _Config, "./bmc-ibm-7.cnf").

test_queue_invar2(_Config) ->
    test_dpll_original( _Config, "./queueinvar2.dimacs").

test_queue_invar4(_Config) ->
    test_dpll_original(_Config, "./queueinvar4.dimacs").

test_queue_invar6(_Config) ->
    test_dpll_original(_Config, "./queueinvar6.dimacs").

test_dpll_original(_Config, Path) ->
    D = custom_data(_Config, Path),
    {T, _} = timer:tc(fun() -> dpll_original:dpll(D) end),
    ct:print(?INFO, "Test name: ~p, Execution time = ~p", [Path, T]),
    ok.
