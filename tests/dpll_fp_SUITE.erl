-module(dpll_fp_SUITE).
-include_lib("common_test/include/ct.hrl").
-include_lib("stdlib/include/assert.hrl").
-define(INFO, ?LOW_IMPORTANCE).
-define(ERROR, ?HI_IMPORTANCE).

-export([all/0
	, init_per_testcase/2
	, end_per_testcase/2
	, suite/0
	]).

-export([ test_ibm_bmc5/1
	, test_ibm_bmc6/1
	, test_ibm_bmc7/1
	, test_queue_invar2/1
	, test_queue_invar4/1
	, test_queue_invar6/1
	]).

all() ->
    [
     test_queue_invar2
    , test_queue_invar4
    , test_bmc_ibm5
    , test_bmc_ibm7
    , test_bmc_ibm6
    , test_queue_invar6
    ]
	
suite() ->
    [{timetrap, {minutes, 60*3}}].

init_per_testcase(_TestCase, Config) ->
    Config.

end_per_testcase(_TestCase, Config) ->
    Config.

custom_data(Config, Path) ->
    {_, _, D} = common:parse_dimacs(
		  ?config(data_dir, Config) ++ Path
		 ),
    D.

test_queue_invar2(_Config) ->
    test_dpll_fp( _Config, "./queueinvar2.dimacs").

test_queue_invar4(_Config) ->
    test_dpll_fp(_Config, "./queueinvar4.dimacs").

test_queue_invar6(_Config) ->
    test_dpll_fp(_Config, "./queueinvar6.dimacs").

test_dpll_fp(_Config, Path) ->
    D = custom_data(_Config, Path),
    D_0 = erlang:external_size(D),
    {T, {Res, FullCert, D_max}} =
	timer:tc( 
	  fun() ->
		fixpoint:fixpoint_cert_test(fun dpll:dpll_fp/1, [{ur, D}])
	  end),
    C_f = length(FullCert),
    ct:print(?INFO, "Test name: ~p, Execution time = ~p, D_0 = ~p, D_max = ~p~n, C_f = ~p", 
	     [Path, T, D_0, D_max, C_f]),
    ok.
